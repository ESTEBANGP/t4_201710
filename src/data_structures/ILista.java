package data_structures;

public interface ILista<T> extends Iterable<T>{
	
	/**
	 * A�ade el elemento elem al final de la lista
	 * @param elem
	 */
	public void agregarElementoFinal(T elem);
	
	/**
	 * A�ade el elemento elem al inicio de la lista
	 * @param elem
	 */
	public void agregarElementoInicio(T elem);
	
	/**
	 * Retorna el elemento en la posici�n pos
	 * @param pos posici�n a buscar en la lista
	 * @return elemento en la posici�n pos
	 */
	public T darElemento(int pos);
	
	/**
	 * Devuelve el tama�o de la lista
	 * @return n�mero de elementos en la lista
	 */
	public int darNumeroElementos();
	
	/**
	 * Devuelve el elemento en la posici�n actual de referencia de la lista
	 * @return elemento - es el elemento guardado en la posci�n actual de referencia de la lista
	 */
	public T darElementoPosicionActual();
	
	/**
	 * Avanza la posici�n actual de referencia en una posici�n
	 * @return true si pudo avanzar, false si es la �ltima posici�n de la lista
	 */
	public boolean avanzarSiguientePosicion();
	
	/**
	 * Retrocede la posici�n actual de referencia en una posici�n 
	 * @return true si pudo retroceder, false de lo contrario.
	 */
	public boolean retrocederPosicionAnterior();
	
	/**
	 * Elimina el primer nodo. 
	 * @return el elemento que esta en el nodo que se elimino
	 */
	public T eliminarPrimero();
	
	/**
	 * Elimina el ultimo nodo. 
	 * @return el elemento que esta en el nodo que se elimino
	 */
	public T eliminarUltimo();
	
	/**
	 * da el primer nodo. 
	 * @return el elemento del primer nodo.
	 */
	public T darElementoPrimero();
	
	/**
	 * da el ultimo nodo. 
	 * @return el elemento del ultimo nodo.
	 */
	public T darElementoUltimo();
	
	public void cambiarDosElementos (int pos1, int pos2);
	
	public NodoDoble<T> darNodo(int pos);
	
	
	
	

}
