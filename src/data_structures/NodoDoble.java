/**
 * 
 */
package data_structures;

/**
 * @author SAMSUNG
 *
 */
public class NodoDoble<T> {
	
	private NodoDoble<T> siguiente;
	private NodoDoble<T> anterior;
	private T elemento;

	
	public NodoDoble(T pElemento){
		elemento=pElemento;
		siguiente=null;
		anterior=null;
	}
	public T darElemento(){
		return elemento;
	}
	
	
	
	public NodoDoble<T> darSiguiente(){
		return siguiente;
	}
	
	public NodoDoble<T> darAnterior(){
		return anterior;
	}
	
	public void modificarSiguiente(NodoDoble<T> nodo){
		siguiente=nodo;

	}
	
	public void modificarAnterior(NodoDoble<T> nodo){
		anterior=nodo;
		
	}
	
	public void agregarElemento(T pElemento){
		elemento= pElemento;
	}
	
	public void modificarT(T nuevo){
		elemento = nuevo;
	}


}
