package data_structures;

import java.util.Iterator;

public class IteratorListaDoble<T> implements Iterator<T>
{
	private NodoDoble<T> actual;
	
	public IteratorListaDoble(NodoDoble<T> pActual){
		actual=pActual;
		
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		if (actual!=null){
			return true;
		}
		return false;
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
	      NodoDoble<T> referencia = actual;
          T item = referencia.darElemento();
          actual = actual.darSiguiente(); 
          return item;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}


}
