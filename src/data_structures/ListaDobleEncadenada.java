package data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T extends Comparable <T>> implements ILista<T>  {


	private NodoDoble<T> primero;
	private NodoDoble<T> ultimo;
	private NodoDoble<T> actual;


	public ListaDobleEncadenada(){
		
		primero=null;
		ultimo=null;


	}


	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteratorListaDoble<T>(primero);
	}

	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub

		NodoDoble<T> elAgregar= new NodoDoble<T>(elem);

		if(primero==null){
			primero= elAgregar;
			ultimo= primero;
		
		}
		else{

			ultimo.modificarSiguiente(elAgregar);
			elAgregar.modificarAnterior(ultimo);
			ultimo=elAgregar;
		
		}

	}
	public void agregarElementoInicio(T elem) {
		// TODO Auto-generated method stub

		NodoDoble<T> elAgregar= new NodoDoble<T>(elem);

		if(primero==null){
			primero= elAgregar;
			ultimo= primero;
	
		}
		else{
            
			elAgregar.modificarSiguiente(primero);
			elAgregar.modificarAnterior(null);
			primero=elAgregar;

		}

	}
	
    public void cambiarDosElementos (int pos1, int pos2){
      T temporal = darElemento(pos1);
      (darNodo(pos1)).modificarT(darElemento(pos2));
      (darNodo(pos2)).modificarT(temporal);
    }
	
	public T darElemento(int pos) {
		// TODO Auto-generated method stub


		int contador=0;
		if (primero==null){
			return null;
		}
		NodoDoble<T>buscado =primero;
		while (primero!=null && contador!=pos){
			buscado=buscado.darSiguiente();
			contador++;
		}

		actual=buscado;

		return buscado.darElemento();
	}
	
	public NodoDoble<T> darNodo(int pos) {
		// TODO Auto-generated method stub


		int contador=0;
		if (primero==null){
			return null;
		}
		NodoDoble<T>buscado =primero;
		while (primero!=null && contador!=pos){
			buscado=buscado.darSiguiente();
			contador++;
		}

		actual=buscado;

		return buscado;
	}
	



	public int darNumeroElementos() {
		// TODO Auto-generated method stub
	NodoDoble<T> ahora=primero;
	int contador=0;
		while (ahora!=null) {
		contador++;
		ahora=ahora.darSiguiente();
		
	}
		return contador;
	}


	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		if (actual==null){
			actual=primero;
			return actual.darElemento();
		}

		return actual.darElemento();

	}


	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub


		if (primero==null){
			return false;
		}

		else{

			if (actual==null){
				actual=primero;
			}

			if (actual.darSiguiente()==null ){
				return false;
			}
			else{
				actual=actual.darSiguiente();
				return true;
			}
		}


	}

	public void moverActualInicio(){

		actual=primero;

	}



	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if (primero!=null){
			return false;
		}
		else{
			if (actual==null){
				actual=primero;
			}

			if (actual.darAnterior()==null ){
				return false;
			}
			else{
				actual=actual.darAnterior();
				return true;
			}
		}

	}


	public T eliminarPrimero() {

         if (darNumeroElementos()<=0){
        	System.out.println("No puede eliminar mas");
         }
         T aux = primero.darElemento();
         primero=primero.darSiguiente();
         return aux;
	}

	public T eliminarUltimo(){

		if (darNumeroElementos()==1){
			NodoDoble<T> referencia=ultimo;
			ultimo=null;
	
			return referencia.darElemento();
		}
		if (darNumeroElementos()>=2){
			NodoDoble<T> referencia=ultimo;
			ultimo=ultimo.darAnterior();
			
			return referencia.darElemento();
		}
		return null;
	}

	public T darElementoPrimero(){
         return primero.darElemento();
	}

	public T darElementoUltimo(){
          return ultimo.darElemento();
	}





}
