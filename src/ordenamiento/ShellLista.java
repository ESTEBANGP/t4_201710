package ordenamiento;

import manejadorPeliculas.Pelicula;
import data_structures.ILista;
import data_structures.ListaDobleEncadenada;
import data_structures.NodoDoble;

public class ShellLista {
	
	public static void sort(ListaDobleEncadenada<Pelicula> lista,int N)
	{
		
		int h = 0;
		while(h < N/3)
		{
			h = 3*h+1;
		}
		while(h >= 1)
		{
			for (int i = h; i < N; i++)
			{
				for (int j = i; j >= h && less(lista.darElemento(j), lista.darElemento(j-h)); j-=h)
				{
					exch(lista,j,j-h);
				}
			}
			h=h/3;
		}
	}

	public static boolean less(Comparable a, Comparable b)
	{
		if (a.compareTo(b)<0)
		{
			return true;
		}

		return false;
	}

	public static void exch(ListaDobleEncadenada<Pelicula> lista, int b, int c )
	{
		lista.cambiarDosElementos(b, c);
	}
}
