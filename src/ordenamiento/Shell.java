package ordenamiento;

public class Shell
{

	public static void sort(Comparable[]a,int N)
	{
		
		int h = 0;
		while(h < N/3)
		{
			h = 3*h+1;
		}
		while(h >= 1)
		{
			for (int i = h; i < N; i++)
			{
				for (int j = i; j >= h && less(a[j], a[j-h]); j-=h)
				{
					exch(a,j,j-h);
				}
			}
			h=h/3;
		}
	}

	public static boolean less(Comparable a, Comparable b)
	{
		if (a.compareTo(b)<0)
		{
			return true;
		}

		return false;
	}

	public static void exch(Comparable[] arreglo, int b, int c )
	{
		Comparable temporal = arreglo[b];
		arreglo[b] = arreglo[c];
		arreglo[c] = temporal;
	}
}
