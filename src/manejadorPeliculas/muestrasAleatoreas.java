package manejadorPeliculas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import ordenamiento.Shell;
import ordenamiento.ShellLista;
import data_structures.ILista;
import data_structures.ListaDobleEncadenada;



public class muestrasAleatoreas {

	private ListaDobleEncadenada<Pelicula> misPeliculas;
	private Comparable<Pelicula> [] arregloComparble;
	private ShellLista ordenadorLista;
	private long guardar;
	private long guardar2;


	public void cargarArchivoPeliculas(String archivoPeliculas, int N) {
		// TODO Auto-generated method stub
		FileReader lector;
		try {	

			int numeroPeli;
			String nombrePeli;
			int agnoCreacion=0;
			lector = new FileReader(archivoPeliculas);	
			BufferedReader input = new BufferedReader(lector);
			misPeliculas = new ListaDobleEncadenada<>();
			ordenadorLista = new ShellLista();
			arregloComparble = new Pelicula [N+1];
			guardar =0L;
			guardar2 =0L;
			



			try {
				String data= input.readLine();
				data=input.readLine();
				int contador=0;
				System.out.println("");
				while (data!=null && contador<=N) {

					ILista<String> generos = new ListaDobleEncadenada<>();
					String mitad= data.substring(data.indexOf(',')+1, data.lastIndexOf(','));
					String ultimo = data.substring(data.lastIndexOf(',')+1, data.length());
					int primero = Integer.parseInt(data.substring(0,data.indexOf(',')));

					int posI=mitad.lastIndexOf(40);
					int posF=mitad.lastIndexOf(41);

					if (posF>=0 && posI>=0 && !(mitad.substring(posI+1, posF)).equals("2007-") && (mitad.substring(posI+1, posF)).length()<5  ){
						agnoCreacion=Integer.parseInt(mitad.substring(posI+1, posF));
					}
					else{
						agnoCreacion=0;
					}



					generos.agregarElementoFinal(ultimo);
					Pelicula obj =new Pelicula();
					obj.setAgnoPublicacion(agnoCreacion);
					//					if (mitad.lastIndexOf('(')>0){
					//					obj.setTitulo(mitad.substring(0,mitad.lastIndexOf('(')));
					//					}
					//					else{
					//						obj.setTitulo(mitad);
					//					}
					obj.setTitulo(mitad);
					obj.setGenerosAsociados(generos);
					misPeliculas.agregarElementoFinal(obj);
					arregloComparble[contador]=obj;

					//	    	System.out.println(mitad);


					contador++;
					data=input.readLine();
					if(data==null && contador<=N){
						input.close();
						lector = new FileReader(archivoPeliculas);	
						BufferedReader input2 = new BufferedReader(lector);
						input=input2;
						input.readLine();
						data=input.readLine();

					}
				}

//				System.out.println("*****************fin***********");
               

			} catch (IOException e) {
				// TODO Auto-generated catch block

				System.out.println("ERROR READ LINE");
			}


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block

			System.out.println("ERROR FILE READER");
		}

	}
	
	public Comparable [] darArreglo ()
	{
		return arregloComparble;
	}
	public long darGuardar()
	{
		return guardar;
	}
	
	public long darGuardar2()
	{
		return guardar2;
	}
	public ListaDobleEncadenada darLista ()
	{
		return misPeliculas;
	}

	public long ordenar(Comparable<Pelicula> [] arreglo, int N){
		long time=System.currentTimeMillis();
		guardar=time;
		Shell.sort(arreglo, N);
		time=System.currentTimeMillis();
	    return (time);
		
	}
	
	
	public long ordenarLista(ListaDobleEncadenada<Pelicula> lista, int N){
		long tiempoO=System.currentTimeMillis();
		guardar2= tiempoO;
		ordenadorLista.sort(lista, N);
		tiempoO = System.currentTimeMillis();
		return (tiempoO);
	}


	public static void main(String[] args) {
		muestrasAleatoreas mostrar= new muestrasAleatoreas();
		mostrar.cargarArchivoPeliculas( "data/movies.csv", (int) Math.pow(2, 10));
		
		Comparable [] arreglo = mostrar.darArreglo();
		ListaDobleEncadenada lista = mostrar.darLista();
		
		long bla=mostrar.ordenar(arreglo, arreglo.length);
		long bla2=mostrar.ordenarLista(lista, lista.darNumeroElementos());
		long tT1= (bla-mostrar.darGuardar());
		long tT2= (bla2-mostrar.darGuardar2());
		
		
		System.out.println( "El tiempo para la lista es: " + tT2);
		
		System.out.println( "El tiempo para el arreglo es: " +  +tT1 );
	}

}
