package manejadorPeliculas;

import data_structures.ILista;

public class Pelicula implements Comparable<Pelicula> {
	
	private String titulo;
	private int agnoPublicacion;
	private ILista<String> generosAsociados;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	
	@Override
	public int compareTo(Pelicula o) {
		// TODO Auto-generated method stub
		return titulo.compareToIgnoreCase(o.titulo);
	}

}
