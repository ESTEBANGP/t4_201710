package pruebasOrdenamiento;

import ordenamiento.ShellLista;
import manejadorPeliculas.Pelicula;
import data_structures.ILista;
import data_structures.ListaDobleEncadenada;
import junit.framework.TestCase;

public class pruebaShellLista extends TestCase {
	
	 private ListaDobleEncadenada<Pelicula> lista;
	 private ShellLista ordenar;
	 
		private void setupEscenario1()
		{
			lista = new ListaDobleEncadenada<>();
			 Pelicula pelicula1 = new Pelicula();
		        pelicula1.setTitulo("Eban");
		        Pelicula pelicula2 = new Pelicula();
		        pelicula2.setTitulo("Dagon");
		        Pelicula pelicula3 = new Pelicula();
		        pelicula3.setTitulo("Crazy");
		        Pelicula pelicula4 = new Pelicula();
		        pelicula4.setTitulo("Baaria");
		        Pelicula pelicula5 = new Pelicula();
		        pelicula5.setTitulo("Abel");
			
			lista.agregarElementoFinal(pelicula1);
			lista.agregarElementoFinal(pelicula2);
			lista.agregarElementoFinal(pelicula3);
			lista.agregarElementoFinal(pelicula4);
			lista.agregarElementoFinal(pelicula5);
			
		}
		
		public void testOrdenar()
		{
			setupEscenario1();
			ListaDobleEncadenada<Pelicula> copia;
			copia = new ListaDobleEncadenada<>();
			
			Pelicula pelicula1 = new Pelicula();
	        pelicula1.setTitulo("Eban");
	        Pelicula pelicula2 = new Pelicula();
	        pelicula2.setTitulo("Dagon");
	        Pelicula pelicula3 = new Pelicula();
	        pelicula3.setTitulo("Crazy");
	        Pelicula pelicula4 = new Pelicula();
	        pelicula4.setTitulo("Baaria");
	        Pelicula pelicula5 = new Pelicula();
	        pelicula5.setTitulo("Abel");
	        
	        copia.agregarElementoFinal(pelicula5);
	        copia.agregarElementoFinal(pelicula4);
	        copia.agregarElementoFinal(pelicula3);
	        copia.agregarElementoFinal(pelicula2);
	        copia.agregarElementoFinal(pelicula1);
	        
	        ordenar.sort(lista, lista.darNumeroElementos());
	        
	        assertEquals( ((Pelicula)(copia.darElemento(0))).getTitulo(), ((Pelicula)(lista.darElemento(0))).getTitulo());
	        assertEquals( ((Pelicula)(copia.darElemento(1))).getTitulo(), ((Pelicula)(lista.darElemento(1))).getTitulo());
	        assertEquals( ((Pelicula)(copia.darElemento(2))).getTitulo(), ((Pelicula)(lista.darElemento(2))).getTitulo());
	        assertEquals( ((Pelicula)(copia.darElemento(3))).getTitulo(), ((Pelicula)(lista.darElemento(3))).getTitulo());
	        assertEquals( ((Pelicula)(copia.darElemento(4))).getTitulo(), ((Pelicula)(lista.darElemento(4))).getTitulo());
	        
		}

}
