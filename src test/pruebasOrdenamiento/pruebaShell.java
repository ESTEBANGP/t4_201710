package pruebasOrdenamiento;


import manejadorPeliculas.Pelicula;
import ordenamiento.Shell;
import junit.framework.TestCase;


public class pruebaShell extends TestCase {

	private Pelicula [] arregloPeliculas;



	private void setupEscenario1()
	{
		
		arregloPeliculas = new Pelicula [5];
        Pelicula pelicula1 = new Pelicula();
        pelicula1.setTitulo("Eban");
        Pelicula pelicula2 = new Pelicula();
        pelicula2.setTitulo("Dagon");
        Pelicula pelicula3 = new Pelicula();
        pelicula3.setTitulo("Crazy");
        Pelicula pelicula4 = new Pelicula();
        pelicula4.setTitulo("Baaria");
        Pelicula pelicula5 = new Pelicula();
        pelicula5.setTitulo("Abel");
        
        arregloPeliculas[0]= pelicula1;
        arregloPeliculas[1]= pelicula2;
        arregloPeliculas[2]= pelicula3;
        arregloPeliculas[3]= pelicula4;
        arregloPeliculas[4]= pelicula5;
		
												

	}

	public void testOrdenar()
	{
		setupEscenario1();
		
		Pelicula[] copia;
		
		copia = new Pelicula [5];
		
		Pelicula pelicula1 = new Pelicula();
        pelicula1.setTitulo("Eban");
        Pelicula pelicula2 = new Pelicula();
        pelicula2.setTitulo("Dagon");
        Pelicula pelicula3 = new Pelicula();
        pelicula3.setTitulo("Crazy");
        Pelicula pelicula4 = new Pelicula();
        pelicula4.setTitulo("Baaria");
        Pelicula pelicula5 = new Pelicula();
        pelicula5.setTitulo("Abel");
		
		
		copia [0] =pelicula5;
		copia [1] =pelicula4;
		copia [2] =pelicula3;
		copia [3] =pelicula2;
		copia [4] =pelicula1;
		
	   Shell.sort(arregloPeliculas, arregloPeliculas.length);


		
		
		assertEquals(copia[0].getTitulo(), arregloPeliculas[0].getTitulo() );
		assertEquals(copia[1].getTitulo(), arregloPeliculas[1].getTitulo() );
		assertEquals(copia[2].getTitulo(), arregloPeliculas[2].getTitulo() );
		assertEquals(copia[3].getTitulo(), arregloPeliculas[3].getTitulo() );
		assertEquals(copia[4].getTitulo(), arregloPeliculas[4].getTitulo() );

	


	}
}
